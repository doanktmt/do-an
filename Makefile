all: fpgrowth.o fptree.o tract.o
	gcc fpgrowth.o fptree.o tract.o -lm -o fpgrowth
fpgrowth.o: fpgrowth.c tract.h fptree.h
	gcc -c fpgrowth.c
fptree.o: fptree.c fptree.h tract.h
	gcc -c fptree.c
tract.o: tract.c tract.h
	gcc -c tract.c
clean:
	rm -f *.o fpgrowth
